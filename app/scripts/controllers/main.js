'use strict';

/**
 * @ngdoc function
 * @name materialDesignApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the materialDesignApp
 */
angular.module('materialDesignApp')
  .controller('MainCtrl', function ($scope) {
   
  	//chat array definition
  	$scope.chatList =[
	  	{
	  		title:'Santhosh',
	  		message:' Secondary line text Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
	  		avatar:'styles/images/avatar.png',
	  		time:"11:00 AM"
	  	},
	  		{
	  		title:'Suresh',
	  		message:'Define and create a single object, using an object literal. Define and create a single ',
	  		avatar:'styles/images/avatar.png',
	  		time:"11:00 AM"
	  	},
	  		{
	  		title:'Suresh',
	  		message:'Define and create a single object, using an object literal. Define and create a single ',
	  		avatar:'styles/images/avatar.png',
	  		time:"11:00 AM"
	  	},
	  		{
	  		title:'Suresh',
	  		message:'Define and create a single object, using an object literal. Define and create a single ',
	  		avatar:'styles/images/avatar.png',
	  		time:"11:00 AM"
	  	},
	  		{
	  		title:'Suresh',
	  		message:'Define and create a single object, using an object literal. Define and create a single ',
	  		avatar:'styles/images/avatar.png',
	  		time:"11:00 AM"
	  	}
  	] 
  	$scope.contactList =[
  	{
  		avatar:"styles/images/avatar.png",
  		name:'Santhosh',
  		number:'9566425325'
  	},
  	{
  		avatar:"styles/images/avatar.png",
  			name:'Santhosh',
  		number:'9566425325'
  	},
  	{
  		avatar:"styles/images/avatar.png",
  			name:'Santhosh',
  		number:'9566425325'
  	},{
  		avatar:"styles/images/avatar.png",
  			name:'Santhosh',
  		number:'9566425325'
  	}
  	];

  	$scope.callList = [{
  		avatar:"styles/images/avatar.png",
  		number:'956610614',
  		time:'11:00 AM',
  		type:'styles/images/call.png'
  	},{
  		avatar:"styles/images/avatar.png",
  		number:'956610614',
  		time:'11:00 AM',
  		type:'styles/images/call.png'
  	},{
  		avatar:"styles/images/avatar.png",
  		number:'956610614',
  		time:'11:00 AM',
  		type:'styles/images/call.png'
  	},{
  		avatar:"styles/images/avatar.png",
  		number:'956610614',
  		time:'11:00 AM',
  		type:'styles/images/call.png'
  	},{
  		avatar:"styles/images/avatar.png",
  		number:'956610614',
  		time:'11:00 AM',
  		type:'styles/images/call.png'
  	}]
  });
